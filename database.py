import psycopg2

con=psycopg2.connect(
        host="demodb",
        database="test",
        user="postgres",
        password="123456")

cur=con.cursor()
cur.execute("select ID, Name from employee")
rows =cur.fetchall()

for r in rows :
        print(f"id{r[0]} name {r[1]}")

cur.close()
con.close()